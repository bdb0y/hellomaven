import com.example.app.Main;
import org.junit.*;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void shouldCalculateFactorial() {

        assertEquals(1, Main.factorial(0));
        assertEquals(24, Main.factorial(4));
        System.out.println("---Factorial Test Passed---");
    }

}
