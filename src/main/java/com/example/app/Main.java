package com.example.app;

public class Main {
    public static void main(String[] args) {

    }

    public static long factorial(long n) {
        if (n == 0) return 0;
        return n * factorial(n - 1);
    }
}
